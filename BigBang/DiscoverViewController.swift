//
//  DiscoverViewController.swift
//  BigBang
//
//  Created by brst on 25/01/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class DiscoverViewController: UIViewController {

    @IBOutlet var fadeview: UIView!
    @IBOutlet var perfumeview: UIView!
    @IBOutlet var bottleview: UIView!
    @IBOutlet var mainview: UIView!
    @IBOutlet var notesview: UIView!
    @IBOutlet var bottleimage: UIImageView!
    @IBOutlet var notesimage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBarHidden = true
        
        
        
        // Do any additional setup after loading the view.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func viewWillAppear(animated: Bool) {
        self.fadeview.hidden = true
//        self.notesview.frame.origin.x =  self.notesview.frame.origin.x + 1000
//        self.notesimage.frame.size.height = 50
//        self.notesimage.frame.size.width = 50
        self.notesimage.alpha = 0
        self.notesview.alpha = 0
        
        
        UIView.animateWithDuration(2.0, animations: {
            self.notesimage.alpha = 2.0
            self.notesview.alpha = 2.0
            
//            self.notesview.frame.origin.x = 242
//            self.notesimage.frame.size.width = 240
//            self.notesview.frame.origin.y = 316
//            self.notesimage.frame.size.height = 177
//            
            
            }) { _ in
        }
        
//        self.bottleview.frame.origin.x =  self.bottleview.frame.origin.x + 1000
//        self.bottleimage.frame.size.height = 50
//        self.bottleimage.frame.size.width = 50
        self.bottleimage.alpha = 0
         self.bottleview.alpha = 0
        UIView.animateWithDuration(3.0, animations: {
            self.bottleimage.alpha = 2.0
            self.bottleview.alpha = 2.0
//            self.bottleview.frame.origin.x = 527
//            self.bottleimage.frame.size.width = 240
//            self.bottleview.frame.origin.y = 136
//            self.bottleimage.frame.size.height = 177
//            
            
            }) { _ in
        }
        
        
        self.mainview.alpha = 0
        
        UIView.animateWithDuration(5.0, animations: {
            
            self.mainview.alpha = 2.0
            
            }) { _ in
            }
            
            
        self.perfumeview.alpha = 0
        
        UIView.animateWithDuration(6.0, animations: {
            
            self.perfumeview.alpha = 2.0
            }) { _ in
        }
    }
    override func viewWillDisappear(animated: Bool) {
        
    
    
        
              
    }
    
    
    @IBAction func backtohome(sender: AnyObject)
    {
       self.navigationController!.popViewControllerAnimated(true)
//self.dismissViewControllerAnimated(true, completion: nil)
        
    }

    @IBAction func slidebar(sender: AnyObject)
    {
        let Slide = self.storyboard!.instantiateViewControllerWithIdentifier("SlideViewController") as! SlideViewController
        
        self.navigationController?.pushViewController(Slide, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func discovervideo(sender: AnyObject) {
        self.fadeview.hidden = false
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            let watch = self.storyboard!.instantiateViewControllerWithIdentifier("DiscovervideoViewController") as! DiscovervideoViewController
            
            self.navigationController?.pushViewController(watch, animated: true)
        }
        

//        let Slide = self.storyboard!.instantiateViewControllerWithIdentifier("DiscovervideoViewController") as! DiscovervideoViewController
//        
//        self.navigationController?.pushViewController(Slide, animated: true)

    }
    

    @IBAction func discovervideos(sender: AnyObject) {
        self.fadeview.hidden = false
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            let watch = self.storyboard!.instantiateViewControllerWithIdentifier("DiscovervideosViewController") as! DiscovervideosViewController
            
            self.navigationController?.pushViewController(watch, animated: true)
        }
        

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
