//
//  WatchmakingViewController.swift
//  BigBang
//
//  Created by brst on 22/01/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class WatchmakingViewController: UIViewController {

    @IBOutlet var fadeview: UIView!
    
    @IBOutlet var directorview: UIView!
    @IBOutlet var makingview: UIView!
    @IBOutlet var watchview: UIView!
    
    @IBOutlet var directorimage: UIImageView!
    @IBOutlet var makingimage: UIImageView!
    @IBOutlet var watchimage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationController!.navigationBarHidden = true
       
       // self.fadeview.hidden = true
       
        
    }

    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func viewWillAppear(animated: Bool)
    {
        //self.fadeview.hidden = true
        self.fadeview.hidden = true

//        self.watchview.frame.origin.x =  self.watchview.frame.origin.x + 1000
//        self.watchimage.frame.size.height = 50
//        self.watchimage.frame.size.width = 50
        self.watchimage.alpha = 0.1
        //self.watchview.alpha = 0.1
        UIView.animateWithDuration(2.0, animations: {
            self.watchimage.alpha = 2.0
//            self.watchview.frame.origin.x = 100
//            self.watchimage.frame.size.width = 257
//            self.watchview.frame.origin.y = 216
//            self.watchimage.frame.size.height = 313
//            
            
            }) { _ in
        }
        
        self.watchview.alpha = 0
        UIView.animateWithDuration(2.0, animations: {
            
            self.watchview.alpha = 2.0
            

            
            }) { _ in
        }
        
//        self.makingview.frame.origin.x =  self.makingview.frame.origin.x + 1000
//        self.makingimage.frame.size.height = 50
//        self.makingimage.frame.size.width = 50
        self.makingimage.alpha = 0
        //self.watchview.alpha = 0.1
        

        UIView.animateWithDuration(3.0, animations: {
            self.makingimage.alpha = 2.0
           // self.watchview.alpha = 2.0
            

//            self.makingview.frame.origin.x = 387
//            self.makingimage.frame.size.width = 252
//            self.makingview.frame.origin.y = 257
//            self.makingimage.frame.size.height = 198
//            
            
            }) { _ in
        }
        self.makingview.alpha = 0
        UIView.animateWithDuration(3.0, animations: {
            
            self.makingview.alpha = 2.0
            
            
            
            }) { _ in
        }
        
//        self.directorview.frame.origin.x =  self.watchview.frame.origin.x + 1000
//        self.directorimage.frame.size.height = 50
//        self.directorimage.frame.size.width = 50
        self.directorimage.alpha = 0
       // self.watchview.alpha = 0.1
        

        UIView.animateWithDuration(4.0, animations: {
            self.directorimage.alpha = 2.0
            //self.watchview.alpha = 2.0
            

//            self.directorview.frame.origin.x = 669
//            self.directorimage.frame.size.width = 257
//            self.directorview.frame.origin.y = 216
//            self.directorimage.frame.size.height = 313
//            
            
            }) { _ in
        }
        
        self.directorview.alpha = 0
        UIView.animateWithDuration(4.0, animations: {
            
            self.directorview.alpha = 2.0
            
            
            
            }) { _ in
        }
        
 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func Backtohome(sender: AnyObject)
    {
        self.navigationController!.popViewControllerAnimated(true)
    }
    

    @IBAction func slidebar(sender: AnyObject)
    {
        let Slide = self.storyboard!.instantiateViewControllerWithIdentifier("SlideViewController") as! SlideViewController
        
        self.navigationController?.pushViewController(Slide, animated: true)
    }
    
    
    @IBAction func Filmmaker(sender: AnyObject)
    {
        let Slide = self.storyboard!.instantiateViewControllerWithIdentifier("FilmmakerViewController") as! FilmmakerViewController
        
        self.navigationController?.pushViewController(Slide, animated: true)
    }
    
    @IBAction func watchmakingvideo(sender: AnyObject)
    {
        self.fadeview.hidden = false

        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            let watch = self.storyboard!.instantiateViewControllerWithIdentifier("WatchmakingvideoViewController") as! WatchmakingvideoViewController
            
            self.navigationController?.pushViewController(watch, animated: true)
                }
        
   
    }
    
    @IBAction func watchmakingvideos(sender: AnyObject)
    {
        self.fadeview.hidden = false
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            let watch = self.storyboard!.instantiateViewControllerWithIdentifier("WatchmakingvideosViewController") as! WatchmakingvideosViewController
            
            self.navigationController?.pushViewController(watch, animated: true)
        }

    }
    

}
