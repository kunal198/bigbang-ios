//
//  WatchmakingvideosViewController.swift
//  BigBang
//
//  Created by brst on 25/01/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
class WatchmakingvideosViewController: UIViewController {
var player = AVPlayer()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }
    override func viewWillAppear(animated: Bool)
    {
        
        let path = NSBundle.mainBundle().pathForResource("IM 13. MAKING-OFF SPOT TV EH_2 ", ofType:"mp4")
        player = AVPlayer(URL: NSURL(fileURLWithPath: path!))
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
        self.view.layer.addSublayer(playerLayer)
        player.seekToTime(kCMTimeZero)
        player.play()
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "playerItemDidReachEnd:",
            name: AVPlayerItemDidPlayToEndTimeNotification,
            object: player.currentItem)

    }
    
    func playerItemDidReachEnd(notification: NSNotification)
    {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        
        player.seekToTime(kCMTimeZero)
        
        
    }
    
    @IBAction func back(sender: AnyObject) {
        player.pause()
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
