//
//  SlideViewController.swift
//  BigBang
//
//  Created by brst on 25/01/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class SlideViewController: UIViewController {

    
    @IBOutlet var homeview: UIView!
    
    @IBOutlet var timelessview: UIView!
    @IBOutlet var noveltyview: UIView!
    
    @IBOutlet var advertisingview: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBarHidden = true

        
    }

    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func viewWillAppear(animated: Bool)
    {
        //self.homeview.frame.origin.y = -300
        
       
        self.homeview.alpha = 0
        UIView.animateWithDuration(3.0, animations: {
            self.homeview.alpha = 2.0
            
//            self.homeview.frame.origin.x = 80
//            self.homeview.frame.size.width = 285
//            self.homeview.frame.origin.y = 196
//            self.homeview.frame.size.height = 325
            

            }) { _ in
                
        }
        
        self.advertisingview.alpha = 0
        UIView.animateWithDuration(4.0, animations: {
            self.advertisingview.alpha = 2.0
            }) { _ in
                
        }
        
        self.noveltyview.alpha = 0
        UIView.animateWithDuration(5.0, animations: {
            self.noveltyview.alpha = 2.0
            }) { _ in
                
        }
        
        
        self.timelessview.alpha = 0
        UIView.animateWithDuration(6.0, animations: {
            self.timelessview.alpha = 2.0
            }) { _ in
                
        }

        
//         self.homeview.frame.origin.y = -100
//        UIView.animateWithDuration(4.0, animations: {
//            
//            
//            self.homeview.frame.origin.x = 316
//            self.homeview.frame.size.width = 393
//            self.homeview.frame.origin.y = 123
//            self.homeview.frame.size.height = 73
//            
//            
//            }) { _ in
//                
//        }

        
//        self.advertisingview.frame.origin.y = 160
//        
//        UIView.animateWithDuration(6.0, animations: {
//            
//            
//            self.advertisingview.frame.origin.x = 203
//            self.advertisingview.frame.size.width = 619
//            self.advertisingview.frame.origin.y = 229
//            self.advertisingview.frame.size.height = 116
//            
//            
//            }) { _ in
//                
//        }
//        self.noveltyview.frame.origin.y = 220
//        
//        UIView.animateWithDuration(8.0, animations: {
//            
//            
//            self.noveltyview.frame.origin.x = 149
//            self.noveltyview.frame.size.width = 726
//            self.noveltyview.frame.origin.y = 353
//            self.noveltyview.frame.size.height = 89
//            
//            
//            }) { _ in
//                
//        }
////
//        self.timelessview.frame.origin.y = 270
//        
//        UIView.animateWithDuration(10.0, animations: {
//            
//            
//            self.timelessview.frame.origin.x = 174
//            self.timelessview.frame.size.width = 676
//            self.timelessview.frame.origin.y = 468
//            self.timelessview.frame.size.height = 76
//            
//            
//            }) { _ in
//                
//        }
//        
//
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    
    @IBAction func gotohome(sender: AnyObject)
    {
        let Time = self.storyboard!.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        
        self.navigationController?.pushViewController(Time, animated: true)
 
    }
    
    
    @IBAction func gotowatchmaking(sender: AnyObject)
    {
        let Time = self.storyboard!.instantiateViewControllerWithIdentifier("WatchmakingViewController") as! WatchmakingViewController
        
        self.navigationController?.pushViewController(Time, animated: true)

    }
    
    @IBAction func gotodiscover(sender: AnyObject)
    {
        let Time = self.storyboard!.instantiateViewControllerWithIdentifier("DiscoverViewController") as! DiscoverViewController
        
        self.navigationController?.pushViewController(Time, animated: true)
    }

    
    @IBAction func gototimeless(sender: AnyObject)
    {
        
        let Time = self.storyboard!.instantiateViewControllerWithIdentifier("TimelessViewController") as! TimelessViewController
        
        self.navigationController?.pushViewController(Time, animated: true)
    }
    
    
    
    }
