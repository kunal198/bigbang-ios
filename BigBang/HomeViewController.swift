//
//  HomeViewController.swift
//  BigBang
//
//  Created by brst on 22/01/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    
    @IBOutlet var ilpview: UIView!

    @IBOutlet var bottleview: UIView!
    @IBOutlet var timelessimage: UIImageView!
    @IBOutlet var advertisingimage: UIImageView!
    @IBOutlet var novalityImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationController!.navigationBarHidden = true
        
       // self.firstView.frame.origin.x = -250
        //self.firstView.frame = CGRectMake(80,203,269,323)
        //self.firstView.frame.size.width = 0
        

        // Do any additional setup after loading the view.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func viewWillAppear(animated: Bool)
    {
        
        
//        self.firstView.frame.origin.x = self.firstView.frame.origin.x + 1000
//
//        self.novalityImage.frame.size.height = 50
//        self.novalityImage.frame.size.width = 50
        self.novalityImage.alpha = 0
        //self.firstView.alpha = 0.1
       
        UIView.animateWithDuration(8.0, animations: {
            self.novalityImage.alpha = 2.0
           // self.firstView.alpha = 2.0
            

//            self.firstView.frame.origin.x = 80
//            self.novalityImage.frame.size.width = 285
//            self.firstView.frame.origin.y = 196
//            self.novalityImage.frame.size.height = 325
//            
            
            
            }) { _ in
        }
        
        self.firstView.alpha = 0
        
        UIView.animateWithDuration(8.0, animations: {
            self.firstView.alpha = 2.0
            

            
            
            }) { _ in
        }

        
//        self.secondview.frame.origin.x = self.secondview.frame.origin.x + 1000
//        
//        self.advertisingimage.frame.size.height = 50
//        self.advertisingimage.frame.size.width = 50
        self.advertisingimage.alpha = 0
        //self.secondview.alpha = 0.1
        

        UIView.animateWithDuration(10.0, animations: {
            self.advertisingimage.alpha = 2.0
            //self.secondview.alpha = 2.0
            

//            self.secondview.frame.origin.x = 394
//            self.advertisingimage.frame.size.width = 211
//            self.secondview.frame.origin.y = 209
//            self.advertisingimage.frame.size.height = 246
//            
            
            }) { _ in
        }
        
        self.secondview.alpha = 0
        
        UIView.animateWithDuration(10.0, animations: {
            self.secondview.alpha = 2.0
            
            
            
            
            }) { _ in
        }
        
//        self.thirdview.frame.origin.x =  self.thirdview.frame.origin.x + 1000
//        self.timelessimage.frame.size.height = 50
//        self.timelessimage.frame.size.width = 50
        self.timelessimage.alpha = 0
       // self.thirdview.alpha = 0
        

        UIView.animateWithDuration(12.0, animations: {
            self.timelessimage.alpha = 2.0
           // self.thirdview.alpha = 2.0
            

//            self.thirdview.frame.origin.x = 688
//            self.timelessimage.frame.size.width = 285
//            self.thirdview.frame.origin.y = 196
//            self.timelessimage.frame.size.height = 319
//            
            
            }) { _ in
        }
        
        self.thirdview.alpha = 0
        
        UIView.animateWithDuration(12.0, animations: {
            self.thirdview.alpha = 2.0
            
            
            
            
            }) { _ in
        }

        self.ilpview.alpha = 0
        UIView.animateWithDuration(7.0, animations: {
            self.ilpview.alpha = 2.0

        
        }) { _ in
        
        }
        
        
    }
    
    
    
    
    
    @IBOutlet var firstView: UIView!
    
    @IBOutlet var secondview: UIView!
    
    
    @IBOutlet var thirdview: UIView!
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
       @IBAction func discover(sender: AnyObject)
    {
//        
//        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DiscoverViewController") as! DiscoverViewController
//        self.presentViewController(viewController, animated: false, completion: nil)
        
        let Disc = self.storyboard!.instantiateViewControllerWithIdentifier("DiscoverViewController") as! DiscoverViewController

        self.navigationController?.pushViewController(Disc, animated: true)

        
    }
    
       
    @IBAction func WatchMAKING(sender: AnyObject)
    {
        let Film = self.storyboard!.instantiateViewControllerWithIdentifier("WatchmakingViewController") as! WatchmakingViewController
        
        self.navigationController?.pushViewController(Film, animated: true)

    }
    
    
    @IBAction func Timeless(sender: AnyObject)
    {
        let Time = self.storyboard!.instantiateViewControllerWithIdentifier("TimelessViewController") as! TimelessViewController
        
        self.navigationController?.pushViewController(Time, animated: true)
    }
    
    
   
    @IBAction func Slidebar(sender: AnyObject)
    {
        let Slide = self.storyboard!.instantiateViewControllerWithIdentifier("SlideViewController") as! SlideViewController
        
        self.navigationController?.pushViewController(Slide, animated: true)

        
    }
    
    
    
}
