//
//  ViewController.swift
//  BigBang
//
//  Created by brst on 22/01/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices

class ViewController: UIViewController,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate

{
    
    
    @IBOutlet var ilpview: UIView!
    @IBOutlet var cameraopen: UIView!
    //var player : AVPlayerItem!
    var imagePicker = UIImagePickerController()
    var postAlert = UIAlertView()
    var player = AVPlayer()
    override func viewDidLoad() {
        super.viewDidLoad()
    self.navigationController!.navigationBarHidden = true
      
        
        
        
        }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func viewWillAppear(animated: Bool)
    {
//        self.ilpview.alpha = 0
//        UIView.animateWithDuration(4.0, animations: {
//            self.ilpview.alpha = 2.0
//            
//            
//            }) { _ in
//                
//        }

        
        
        
        
        
        let path = NSBundle.mainBundle().pathForResource("Backgroundvideo", ofType:"mp4")
         player = AVPlayer(URL: NSURL(fileURLWithPath: path!))
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.view.layer.addSublayer(playerLayer)
        player.seekToTime(kCMTimeZero)
        player.play()
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "playerItemDidReachEnd:",
            name: AVPlayerItemDidPlayToEndTimeNotification,
            object: player.currentItem)
        }
    
    func playerItemDidReachEnd(notification: NSNotification)
    {
         let player : AVPlayerItem = notification.object as! AVPlayerItem
        
        player.seekToTime(kCMTimeZero)
        
        
    }

    
    @IBAction func next(sender: AnyObject)
    {
        player.pause()
        let Home = self.storyboard!.instantiateViewControllerWithIdentifier("congralutionViewController") as! congralutionViewController
        
        self.navigationController?.pushViewController(Home, animated: true)
        

    }
    
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

